package beans;

import java.util.Random;

public class Ruleta {
	public static void main(String[] args) {

		Random aleatorio = new Random(System.currentTimeMillis());
		int[] numeros = { 15, 30, 46 };
		int intAletorio = 0;
		boolean ganador = false;

		do {

			// Producir nuevo int aleatorio entre 0 y 99
			intAletorio = aleatorio.nextInt(100);

			System.out.print(intAletorio+"-");
			
			for (int i = 0; i < numeros.length; i++) {
				if (intAletorio == numeros[i]) {
					ganador = true;
					break;
				}
			}
			aleatorio.setSeed(aleatorio.nextLong());
		} while (!ganador);

		System.out.println();
		System.out.println("|----------------------------------------|");
		System.out.println("|    ¡¡FELICIDADES ACABAS DE GANAR!!     |");
		System.out.println("|                  " + intAletorio + "                    |");
		System.out.println("|----------------------------------------|");

		// Refrescar datos aleatorios
		// aleatorio.setSeed(System.currentTimeMillis());
		// ... o mejor
		// aleatorio.setSeed(aleatorio.nextLong());
	}

}
