package beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchWords {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		getFilesNameToSearch();
		//getFilesDuplicados();

	}

	public static void getFilesNameToSearch() throws FileNotFoundException, IOException {

		List<String> filesNotMatch = new ArrayList<String>();
		List<String> filesMatch = new ArrayList<String>();

		String name;
		File f = new File("C:\\clases_existentes.txt");
		FileReader reader = new FileReader(f);
		BufferedReader b = new BufferedReader(reader);

		int encontrado = 0;

		while ((name = b.readLine()) != null) {

			encontrado = searchMatches(name.toLowerCase().trim());

			if (encontrado > 0) {
				filesMatch.add(name + "," + encontrado);

			} else {
				filesNotMatch.add(name);
			}
		}

		b.close();

		System.out.println("Encontrados: " + filesMatch.size() + "-" + filesMatch.toString());
		System.out.println("---------------------------------------------------");
//		System.out.println("No encontrados: " + filesNotMatch.size() + "-" + filesNotMatch);
//		System.out.println("---------------------------------------------------");

		 for (String item : filesNotMatch) {
		 System.out.println("" + item);
		 }
	}

	public static int searchMatches(String name) throws IOException {

		String texto;
		File f = new File("C:\\vane.txt");
		FileReader reader = new FileReader(f);
		BufferedReader b = new BufferedReader(reader);

		int encontrado = 0;
		while ((texto = b.readLine()) != null) {

			// escapar y agregar limites de palabra completa - case-insensitive
			Pattern regex = Pattern.compile("\\b" + Pattern.quote(name) + "\\b", Pattern.CASE_INSENSITIVE);
			Matcher match = regex.matcher(texto.toLowerCase());

			while (match.find()) {
				encontrado++;
			}

		}

		b.close();

		return encontrado;
	}

	public static void getFilesDuplicados() throws FileNotFoundException, IOException {

		List<String> filesNotMatch = new ArrayList<String>();
		List<String> filesMatch = new ArrayList<String>();

		String name, name2;
		File f = new File("C:\\files.txt");
		FileReader reader = new FileReader(f);
		BufferedReader b = new BufferedReader(reader);

		
		while ((name = b.readLine()) != null) {
			
			int encontrado = 0;

			FileReader reader2 = new FileReader(f);
			BufferedReader b2 = new BufferedReader(reader2);

			while ((name2 = b2.readLine()) != null) {

				Pattern regex = Pattern.compile("\\b" + Pattern.quote(name.toLowerCase()) + "\\b",
						Pattern.CASE_INSENSITIVE);
				Matcher match = regex.matcher(name2.toLowerCase());

				while (match.find()) {
					encontrado++;
				}

			}

			if (encontrado > 1) {
				System.out.println(name + ": " + encontrado);
			}

			b2.close();

		}
		b.close();
	}
}
